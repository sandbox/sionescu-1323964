<?php

/**
 * Implementation of hook_features_export().
 */
function features_extra_block_visibility_features_export($data, &$export, $module_name) {
  $pipe = array ();
  $export['dependencies']['features_extra_block_visibility'] = 'features_extra_block_visibility';
  
  $component = 'features_extra_block_visibility';
  foreach ($data as $component) {
    $export['features']['features_extra_block_visibility'][$component] = $component;
    
    if (strpos($component, 'block-') === 0) {
      $machine_name = substr($component, strlen('block-'));
      $pipe['fe_block_boxes'][$machine_name] = $machine_name;
    } else {
      $pipe['block'][$component] = $component;
    }
  }
  
  return $pipe;
}

/**
 * Implementation of hook_features_export_options().
 */
function features_extra_block_visibility_features_export_options() {
  $options = array ();
  
  $blocks = _block_rehash();
  foreach ($blocks as $block) {
    $blockid = _fe_block_build_id($block);
    $options[$blockid] = '[' . $blockid . '] ' . $block['info'];
  }
  
  return $options;
}
/**
 * Implementation of hook_features_export_render()
 */
function features_extra_block_visibility_features_export_render($module_name, $data, $export = NULL) {
  $code = array ();
  $code[] = '$features_extra_block_visibility = array();';
  foreach ($data as $name) {
    $code[] = "  \$features_extra_block_visibility['{$name}'] = " . features_var_export(features_extra_block_visibility_load($name)) . ";";
  }
  $code[] = "return \$features_extra_block_visibility;";
  $code = implode("\n", $code);
  return array ('features_extra_block_visibility_defaults' => $code);
}

/**
 * Load block settings by name
 */
function features_extra_block_visibility_load($name) {
  $visibility = array();
  
  list ($module, $delta) = explode('-', $name);
  if ($module === 'block') {    
    $select = db_select('fe_block_boxes', 'fbb')->fields('fbb', array ('bid'));
    $result = $select->condition('fbb.machine_name', $delta)->execute();
    
    $row = $result
      ->fetchObject();
    
    $delta = $row->bid;
  }
  $select = db_select('block_role', 'br')
    ->fields('r', array('name'));
  $select->join('role', 'r', 'r.rid = br.rid');
  $result = $select->condition('br.module', $module)
    ->condition('br.delta', $delta)
    ->execute();

  $visibility['block_role'] = $result->fetchCol();
  
  $visibility['block_node_type'] = db_select('block_node_type', 'bnt')
    ->fields('bnt', array('type'))
    ->condition('bnt.module', $module)
    ->condition('bnt.delta', $delta)
    ->execute()
    ->fetchCol();
  
  return $visibility;
}

/**
 * Implements hook_features_export_revert()
 */
function features_extra_block_visibility_features_revert($module_name) {
  $mycomponents = features_get_default('features_extra_block_visibility', $module_name);
  if (!empty($mycomponents)) {
    foreach ($mycomponents as $machine_name => $visibility) {
      list ($module, $delta) = explode('-', $machine_name);
      if ($module === 'block') {    
        $select = db_select('fe_block_boxes', 'fbb')->fields('fbb', array ('bid'));
        $result = $select->condition('fbb.machine_name', $delta)->execute();
        
        $row = $result
          ->fetchObject();
        
        $delta = $row->bid;
      }
      foreach ($visibility['block_role'] as $role){
        $role = user_role_load_by_name($role);
        db_merge('block_role')
          ->key(array(
              'rid' => $role->rid,
              'module' => $module,
              'delta' => $delta
            )
          )
          ->fields(array(
              'rid' => $role->rid,
              'module' => $module,
              'delta' => $delta
            )
          )
          ->execute();
      }
      foreach ($visibility['block_node_type'] as $node_type){
        db_merge('block_node_type')
          ->key(array(
              'type' => $node_type,
              'module' => $module,
              'delta' => $delta
            )
          )
          ->fields(array(
              'type' => $node_type,
              'module' => $module,
              'delta' => $delta
            )
          )
          ->execute();
      }
    }
  }
}